import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IndexComponent} from './index/index.component';
import {DeviceViewComponent} from './device-view/device-view.component';


const routes: Routes = [
  {
    path: 'index',
    component: IndexComponent
  },
  {
    path: 'device/:id',
    component: DeviceViewComponent
  },
  {
    path: '**',
    redirectTo: 'index'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
