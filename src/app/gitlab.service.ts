import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GitlabService {

  private GITLAB_API_URL = 'https://jagged-assembly.000webhostapp.com';

  constructor(private httpClient: HttpClient) {
  }

  public getPipelines() {
    return this.httpClient.get(this.GITLAB_API_URL);
  }
}
